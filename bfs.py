from node import *
from collections import deque
import time
from heapq import *
start = time.time()

fs = [[0,1,2],[3,4,5],[6,7,8]] # Final State or Goal State
initial_state = [[7,2,4],[5,0,6],[8,3,1]] # 7 2 4 5 0 6 8 3 1 Inital Configuration
memory = 0


def breath_frist_search(initial_state, final_state):
    counter = 0
    parent = Parent_Node(initial_state)
    if final_state == parent.state:
        return parent.state
    qeue = deque([parent])
    explored = set()

    while True:
        counter += 1
        if not qeue:
            return False
        node = qeue.popleft()
        neighbors = node.get_neighbors(node.state)
        explored.add(node.state_str)
        for neighbor in neighbors:
            child_node = Node(node, neighbor, node.cost)
            if child_node.state_str not in explored:
                if child_node.state_str == str(final_state):

                    print("Yiha!!!")
                    print("Levels: {}".format(child_node.cost))
                    print("Visited nodes: {}".format(len(explored)))
                    return (child_node, counter)

                qeue.append(child_node)

child, memory = breath_frist_search(initial_state,fs)


def backtrace(child):

    current_node = child
    actions = []

    while current_node.parent_node != "Initial":
         actions.insert(0, current_node.action)
         current_node = current_node.parent_node


    return actions

print(child)
print(backtrace(child))
end = time.time()
print("Total Time in seconds: {}".format(end - start))
print("Memory Used: {} bytes".format(memory * 72))
