
# Breath First Search with python

The problem this program solves is a 8-puzzle-piece with an initial state and finds the path to the goal state. to run de program first change directory to project folder

```bash
$ cd ~/bfs-sistemas-inteligentes/
```
Then run the program with 
```bash
$ python3 bfs.py
```
Check your version of python to make sure is python3 or above. 

The initial configuration of the project is as follows. The `fs` variable is the goal state, that is the node which state is the final state that we want to arrive at. `initial_state` is the starting state of the puzzle. The goal of the program is to find a node that contains the `fs` state starting from `initial_state` the program will print to the console the following results with the following configuration:
## Configuration
```python
fs = [[0,1,2],[3,4,5],[6,7,8]] # Final State or Goal State
initial_state = [[7,2,4],[5,0,6],[8,3,1]] # 7 2 4 5 0 6 8 3 1 Inital Configuration
```
## Will print
| Logs        | Values           |
| ------------- |:-------------:|
| Levels      | 27 |
| Visited Nodes    | 161,001     |
| Path to solution | ['left', 'up', 'right', 'down', 'down', 'left', 'up', 'right', 'right', 'up', 'left', 'left', 'down', 'right', 'right', 'down', 'left', 'up', 'right', 'up', 'left', 'down', 'down', 'left', 'up', 'up']     |

You can test that the program works by trying to modify `the initial_state` values by trying clicking the following link:

[8 Puzzle game online](http://www.tilepuzzles.com/default.asp?p=12)

Then you can set up the initial values in the game in `initial_state` and follow the path to solve the puzzle.


# A* with python

The problem this program solves is a 8-puzzle-piece with an initial state and finds the path to the goal state. to run de program first change directory to project folder

```bash
$ cd ~/bfs-sistemas-inteligentes/
```
Then run the program with 
```bash
$ python3 a-star.py
```
Check your version of python to make sure is python3 or above. 

The initial configuration of the project is as follows. The `fs` variable is the goal state, that is the node which state is the final state that we want to arrive at. `initial_state` is the starting state of the puzzle. The goal of the program is to find a node that contains the `fs` state starting from `initial_state` the program will print to the console the following results with the following configuration:
## Configuration
```python
fs = [[0,1,2],[3,4,5],[6,7,8]] # Final State or Goal State
initial_state = [[7,2,4],[5,0,6],[8,3,1]] # 7 2 4 5 0 6 8 3 1 Inital Configuration
```
## Will log

```bash
Yiha!!!
Levels: 26
Visited nodes: 2928
{'parent_node': <node.Node object at 0x106ab0eb8>, 'state': [[0, 1, 2], [3, 4, 5], [6, 7, 8]], 'action': 'up', 'cost': 26, 'possible_actions': ['down', 'right'], 'state_str': '[[0, 1, 2], [3, 4, 5], [6, 7, 8]]', 'map': '[0, 1, 2][3, 4, 5][6, 7, 8]', 'value': 26}
['left', 'up', 'right', 'down', 'down', 'left', 'up', 'right', 'right', 'up', 'left', 'left', 'down', 'right', 'right', 'down', 'left', 'left', 'up', 'right', 'right', 'up', 'left', 'down', 'left', 'up']
Total Time in seconds: 0.3809089660644531
Memory Used: 217008 bytes

Process finished with exit code 0
```

You can test that the program works by trying to modify `the initial_state` values by trying clicking the following link:

[8 Puzzle game online](http://www.tilepuzzles.com/default.asp?p=12)

Then you can set up the initial values in the game in `initial_state` and follow the path to solve the puzzle.

