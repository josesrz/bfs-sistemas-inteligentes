from node import *
import time
from heapq import *
start = time.time()

fs = [[0,1,2],[3,4,5],[6,7,8]] # Final State or Goal State
initial_state = [[7,2,4],[5,0,6],[8,3,1]] # 7 2 4 5 0 6 8 3 1 Inital Configuration

values = {
    2: (1,3),
    5: (2,3),
    8: (3,3),
    13: (1,2),
    16: (2,3),
    19: (3,2),
    24: (1,1),
    27: (2,1),
    30: (3,1)
}

def  a_star(initial_state, final_state):
    counter = 0
    node = Parent_Node(initial_state)
    heap = []

    if final_state == node.state:
        return node.state

    explored = set()
    heappush(heap,(node.value, node))

    while True:
        counter += 1
        if not heap:
            return False

        node = heappop(heap)
        # node[1].get_value(final_state)
        #node[1].value = h(node[1].state_str, str(final_state)) + node[1].cost
        neighbors = node[1].get_neighbors(node[1].state)
        explored.add(node[1].state_str)
        for neighbor in neighbors:
            child_node = Node(node[1], neighbor, node[1].cost)
            child_node.value = h(child_node.state_str, str(final_state)) + child_node.cost
            if child_node.state_str not in explored:
                if child_node.state_str == str(final_state):

                    print("Yiha!!!")
                    print("Levels: {}".format(child_node.cost))
                    print("Visited nodes: {}".format(len(explored)))
                    return (child_node, counter)

                heappush(heap, (child_node.value, child_node))

## Function that will receive a state and node to calculate cost and assign to Node state Humming Method
def calculate_cost(node, initial_State):
    cost = 0
    for i in initial_state:
        for j in i:
            if node.state[i][j] == initial_state[i][j]:
                cost += 0
            else:
                cost += 1
    return cost + node.cost

# Manhattan Heuristic
def h(state, initial_state_str):
    sum = 0
    for i in state:
        index = state.index(i)
        index_two = initial_state_str.index(i)
        if index in values:
            sum += abs(values[index][0] - values[index_two][0]) + abs(values[index][1] - values[index_two][1])

    return sum

def backtrace(child):

    current_node = child
    actions = []

    while current_node.parent_node != "Initial":
         actions.insert(0, current_node.action)
         current_node = current_node.parent_node


    return actions

child, memory= a_star(initial_state, fs)
print(child)
print(backtrace(child))
end = time.time()
print("Total Time in seconds: {}".format(end - start))
print("Memory Used: {} bytes".format(memory * 72))