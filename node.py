from copy import copy, deepcopy


class Parent_Node:

    state = []
    possible_actions = []
    action = "result"
    parent_node = "Initial"
    value = 0

    def __init__(self, state):
        self.state = deepcopy(state)
        self.possible_actions = self.get_neighbors(self.state)
        self.state_str = str(self.state)
        self.cost = 0

        if self.state:
            self.map = ''.join(str(e) for e in self.state)

    def __lt__(self, other):
        return self.map < other.map

    def get_neighbors(self, state):
        possible_states = []
        for i in state:
            for j in i:
                if j == 0:
                    if state.index(i) > 0:
                        possible_states.append('up')
                    if state.index(i) < 2:
                        possible_states.append('down')
                    if i.index(j) < 2:
                        possible_states.append('right')
                    if i.index(j) > 0:
                        possible_states.append('left')
                    break

        return possible_states

    def get_value(self, goal_state):

        cost = 0
        i = 0

        for i in goal_state:
            index = goal_state.index(i)
            if i != "," and i != "[" and i != "]":
                if goal_state[index] == self.state_str[index]:
                    continue
                else:
                    cost += 1

        self.value = cost + self.cost


class Node:

    action = ""
    cost = 1
    # Constructor de nodo que sabe dibujarse
    def __init__(self, node, action, cost):
        self.parent_node = node
        self.state = deepcopy(node.state)
        self.action = action
        self.cost += cost

        index = 0
        index_array = 0
        b_item = 0

        for i in self.state:
            for j in i:
                if j == 0:
                    index_array = self.state.index(i)
                    index = i.index(j)
                    break
        if self.action == 'up':
            b_item = self.state[index_array - 1][index]
            self.state[index_array - 1][index] = 0
            self.state[index_array][index] = b_item
        elif self.action == 'down':
            b_item = self.state[index_array + 1][index]
            self.state[index_array + 1][index] = 0
            self.state[index_array][index] = b_item
        elif self.action == 'left':
            b_item = self.state[index_array][index - 1]
            self.state[index_array][index - 1] = 0
            self.state[index_array][index] = b_item
        elif self.action == 'right':
            b_item = self.state[index_array][index + 1]
            self.state[index_array][index + 1] = 0
            self.state[index_array][index] = b_item
        else:
            print("error")

        self.possible_actions = self.get_neighbors(self.state)
        self.state_str = str(self.state)

        if self.state:
            self.map = ''.join(str(e) for e in self.state)

    def __str__(self):
        return str(self.__dict__)

    def __lt__(self, other):
        return self.map < other.map



    def  get_neighbors(self, state):
        possible_states = []
        for i in state:
            for j in i:
                if j == 0:
                    if state.index(i) > 0:
                        possible_states.append('up')
                    if state.index(i) < 2:
                        possible_states.append('down')
                    if i.index(j) < 2:
                        possible_states.append('right')
                    if i.index(j) > 0:
                        possible_states.append('left')


        return possible_states

    def get_value(self, goal_state):

        h = 0

        for i in goal_state:
            index = goal_state.index(i)
            if i != "," and i != "[" and i != "]":
                if goal_state[index] == self.state_str[index]:
                    continue
                else:
                    h += 1

        self.value = h + self.cost



